# Sequential N-queens using recursive backtracking
Required implementation time: 70 minutes

Adapted [source](https://de.wikipedia.org/wiki/Damenproblem):
```python
    # Erzeuge eine Liste von Lösung auf einem Brett mit Reihen und Spalten.
    # Eine Lösung wird durch eine Liste der Spaltenpositionen,
    # indiziert durch die Reihennummer, angegeben.
    # Die Indizes beginnen mit Null.
    def damenproblem(reihen, spalten):
        if reihen <= 0:
            return [[]] # keine Dame zu setzen; leeres Brett ist Lösung
        else:
            return eine_dame_dazu(reihen - 1, spalten, damenproblem(reihen - 1, spalten))
    
    # Probiere alle Spalten, in denen für eine gegebene Teillösung
    # eine Dame in "neue_reihe" gestellt werden kann.
    # Wenn kein Konflikt mit der Teillösung auftritt,
    # ist eine neue Lösung des um eine Reihe erweiterten
    # Bretts gefunden.
    def eine_dame_dazu(neue_reihe, spalten, vorherige_loesungen):
        neue_loesungen = []
        for loesung in vorherige_loesungen:
            # Versuche, eine Dame in jeder Spalte von neue_reihe einzufügen.
            for neue_spalte in range(spalten):
                # print('Versuch: %s in Reihe %s' % (neue_spalte, neue_reihe))
                if kein_konflikt(neue_reihe, neue_spalte, loesung):
                    # Kein Konflikte, also ist dieser Versuch eine Lösung.
                    neue_loesungen.append(loesung + [neue_spalte])
        return neue_loesungen
    
    # Kann eine Dame an die Position "neue_spalte"/"neue_reihe" gestellt werden,
    # ohne dass sie eine der schon stehenden Damen schlagen kann?
    def kein_konflikt(neue_reihe, neue_spalte, loesung):
        # Stelle sicher, dass die neue Dame mit keiner der existierenden
        # Damen auf einer Spalte oder Diagonalen steht.
        for reihe in range(neue_reihe):
            if (loesung[reihe]         == neue_spalte              or  # gleiche Spalte
                loesung[reihe] + reihe == neue_spalte + neue_reihe or  # gleiche Diagonale
                loesung[reihe] - reihe == neue_spalte - neue_reihe):   # gleiche Diagonale
                    return False
        return True
    
    for loesung in damenproblem(8, 8):
        print(loesung)
```

# OpenMP N-queens using recursive backtracking
Required implementation time: 45 minutes
Required changes:
+ Refactoring for each loops to for loops
+ Refactoring structure for solutions outside the loops in order to make them private for each thread
+ Defining a critical section for the new_solutions
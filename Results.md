## Benchmark results

### N-Queens

#### Changes:

Use read only from vector for placement check [see commit](https://gitlab.com/UIBK_ParSysWS1819/exercise05/commit/288d4f0e3cfbc40af0d1d480029e9c3ac001c11b): 
```cpp

	    -: bool is_safe(int x, int y,  std::vector <int> solution) const {
	    +: bool is_safe(int x, int y,  const std::vector <int> &solution) const {
```

*Different parallel code* in order to resolve data dependencies:
In the previous code version we passed the whole list of possible current solution and computed one new row based on the set of all previous solutions per step.
So that in each iteration there were all results until a certain amount of rows.

Now we only pass the subtree of queens[] based on one starting position in the first row.
The positions in the first row are unrolled beforehand and starting from this position on each result-set is computed
independent from the overall solution set.
[see commit](https://gitlab.com/UIBK_ParSysWS1819/exercise05/commit/b82b0734529e945f21e6b560bd9391b21d12606b):

```cpp
    -: addQueen(int new_row, int columnn, std::vector<std::vector <int>> previous_solutions){
    
    +: // all possible start configurations
       for (auto i = 0; i < column; i++) {
           std::vector<int> queens;
           queens.resize(problem_size, -1);
           addQueen(0, i, queens);
    ...
    +: addQueen(int row, int columnn, std::vector <int> &queens){

```


#### Time Results on the cluster:

GCC-optimized:

|Size|Seq|1T|2T|4T|8T|
|---:|---:|---:|---:|---:|---:|
|8|0 ms|0 ms|0 ms|0 ms|0 ms|
|10|11 ms|5 ms|3 ms|2 ms|2 ms|
|12|347 ms|159 ms|124 ms|59 ms|38 ms|

GCC-previous exercise:

|Size|Seq|1T|2T|4T|8T|
|---:|---:|---:|---:|---:|---:|
|8|1 ms|1 ms|3 ms|3 ms|4 ms|
|10|31 ms|28 ms|61 ms|59 ms|80 ms|
|12|904 ms|841 ms|1650 ms|1512 ms|1830 ms|

ICC-optimized:

|Size|Seq|1T|2T|4T|8T|
|---:|---:|---:|---:|---:|---:|
|8|0 ms|0 ms|0 ms|0 ms|0 ms|
|10|12 ms|8 ms|4 ms|3 ms|3 ms|
|12|374 ms|254 ms|130 ms|74 ms|57 ms|

ICC-previous exercise:

|Size|Seq|1T|2T|4T|8T|
|---:|---:|---:|---:|---:|---:|
|8|1 ms|1 ms|3 ms|2 ms|2 ms|
|10|35 ms|34 ms|64 ms|59 ms|48 ms|
|12|1029 ms|1020 ms|1754 ms|1547 ms|1178 ms|

### Mergesort
Parallelization improvements:

For this algorithm we removed the encapsulation within a class. It's now just a source file
with some functions. Additionally, we introduced some cutoff values for task generation and
changing the sorting algorithm for smaller sub-arrays. The current values for `CUTOFF_MERGESORT`
is 100 000 and the value for `CUTOFF_TASKS` 10 000. They seem to be pretty nice due to some
tries we made. We also changed the three while loops in the merging function to one for loop. Generally
we tried to use references for the parameters where possible to avoid unnecessary coping. The merge function
also now gets the result vector as parameter and not returning it to the caller. We now also use `int32_t`
instead of `int` as it was wanted in the exercise sheet.

GCC - Original Results:

|Size|Seq|1T|2T|4T|8T|
|---:|---:|---:|---:|---:|---:|
|100|0 ms|0 ms|0 ms|0 ms|0 ms|
|1000|0 ms|0 ms|2 ms|3 ms|6 ms|
|10000|2 ms|7 ms|23 ms|36 ms|66 ms|
|100000|29 ms|76 ms|243 ms|318 ms|691 ms|
|1000000|306 ms|772 ms|2427 ms|4476 ms|7646 ms|
|10000000|3263 ms|7806 ms|24071 ms|56940 ms|102597 ms|
|100000000|35702 ms|82528 ms|-|-|-|

We didn't do the last problem size completely here because it already took too long for the
present ones when running with 7 runs for building the median.

GCC - Optimized Results:

|Size|Seq|1T|2T|4T|8T|
|---:|---:|---:|---:|---:|---:|
|100|0 ms|0 ms|0 ms|0 ms|0 ms|
|1000|0 ms|0 ms|0 ms|0 ms|0 ms|
|10000|0 ms|0 ms|0 ms|0 ms|0 ms|
|100000|4 ms|4 ms|4 ms|4 ms|5 ms|
|1000000|61 ms|61 ms|33 ms|28 ms|21 ms|
|10000000|790 ms|793 ms|441 ms|338 ms|249 ms|
|100000000|9779 ms|9809 ms|5580 ms|3847 ms|3455 ms|

As you can see, the optimizations made the program a lot faster. Also the speed increases with increasing
number of threads. In the previous version it got somehow slower the more threads were used.

ICC - Original Results:

|Size|Seq|1T|2T|4T|8T|
|---:|---:|---:|---:|---:|---:|
|100|0 ms|0 ms|0 ms|0 ms|0 ms|
|1000|0 ms|0 ms|0 ms|0 ms|0 ms|
|10000|3 ms|5 ms|6 ms|5 ms|4 ms|
|100000|31 ms|55 ms|116 ms|56 ms|36 ms|
|1000000|335 ms|543 ms|1117 ms|741 ms|435 ms|
|10000000|3564 ms|5783 ms|11080 ms|7030 ms|4335 ms|
|100000000|38148 ms|58823 ms|112173 ms|65713 ms|39497 ms|

ICC - Optimized Results:

|Size|Seq|1T|2T|4T|8T|
|---:|---:|---:|---:|---:|---:|
|100|0 ms|0 ms|0 ms|0 ms|0 ms|
|1000|0 ms|0 ms|0 ms|0 ms|0 ms|
|10000|0 ms|0 ms|0 ms|0 ms|0 ms|
|100000|4 ms|4 ms|4 ms|4 ms|4 ms|
|1000000|57 ms|60 ms|35 ms|27 ms|42 ms|
|10000000|754 ms|788 ms|455 ms|331 ms|497 ms|
|100000000|9440 ms|9847 ms|6042 ms|4213 ms|6039 ms|

The optimized icc version was also a lot faster than the original one. At some points it was also a bit
faster than the gcc version, but sometimes it was also a bit slower. Furthermore, in the original version
it seems to get slower until using 2 threads. From there it then gets faster again, but still slower than
the sequential version.

### Monte Carlo PI estimator

Parallelization improvements:

For optimization we focused on the generation of random numbers.
Since our parallelized version was already using reduction on the number of hits,
we didn't find any additional parallel improvements to do.

We originally were using the `rand` function to generate random numbers.
This was fine for the sequential version, but performed horribly for the
parallel version as the function is blocking on parallel calls.

We then tested some random engines from the C++ Standard random library
against each other. Source code for that can be found at [rng.cpp](/PI_estimator/rng.cpp),
results at [output_rng.txt](/PI_estimator/output_rng.txt). On our local machines the default engine
and the `mt19937_64` engine performed the best. On the cluster however
the `mt19937` engine performed horribly to the other ones.
The winner on the cluster was the `ranlux48_base` engine,
performing a bit better then the default engine.
Thus we chose that one for our parallel version.
We also tried out using a intel only assembler command from
[intel.com](https://software.intel.com/en-us/articles/intel-digital-random-number-generator-drng-software-implementation-guide)
but it did not perform as well as the engines and did not even work on the cluster.

On the sequential version however the `rand_s` function outperformed the engines on the cluster,
thus we chose it for the sequential version of the program.

\
Results on the cluster:

|Size|Seq|1T|2T|4T|8T|
|---:|---:|---:|---:|---:|---:|
|10|0 ms|0 ms|0 ms|0 ms|0 ms|
|100|0 ms|0 ms|0 ms|0 ms|0 ms|
|1000|0 ms|0 ms|0 ms|0 ms|0 ms|
|10000|0 ms|0 ms|0 ms|0 ms|0 ms|
|100000|1 ms|2 ms|1 ms|0 ms|0 ms|
|1000000|15 ms|24 ms|12 ms|6 ms|3 ms|
|10000000|150 ms|248 ms|124 ms|62 ms|31 ms|
|100000000|1508 ms|2489 ms|1246 ms|623 ms|312 ms|
|200000000|2999 ms|4978 ms|2492 ms|1246 ms|625 ms|
|500000000|7515 ms|12443 ms|6230 ms|3117 ms|1561 ms|

We can see that the parallel versions achieve very good speedup compared
to the sequential version. Compared to each other they always perform twice as fast with twice
as many threads.

Additionally we can see that the parallel version with one thread performs
worse then the sequential version, since we are using different random number generators for them.

We also tried with the intel icc compiler, but the results were the same.

\
Results for the not optimized code from the last exercise on the cluster:

|Size|Seq|1T|2T|4T|8T|
|---:|---:|---:|---:|---:|---:|
|10|0 ms|0 ms|0 ms|0 ms|0 ms|
|100|0 ms|0 ms|0 ms|0 ms|0 ms|
|1000|0 ms|0 ms|1 ms|0 ms|1 ms|
|10000|0 ms|0 ms|10 ms|6 ms|11 ms|
|100000|2 ms|2 ms|102 ms|77 ms|108 ms|
|1000000|21 ms|28 ms|1289 ms|729 ms|1123 ms|
|10000000|217 ms|287 ms|14464 ms|8492 ms|11158 ms|
|100000000|2170 ms|-|-|-|-|
|200000000|4340 ms|-|-|-|-|

We did not go as far down for the parallel version, since they are really slow due to the blocking
nature of the `rand` function. Compared to the optimized code we see improvements in
the sequential and the parralel version due to using other random number generators.


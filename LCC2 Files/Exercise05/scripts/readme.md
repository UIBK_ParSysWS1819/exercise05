### Running jobs on the cluster

1. Use scp to get the necessary files on the cluster

On the cluster:

2. compile files (Use -std=c++11)

3. `chmod 777 bench.sh`

4. `qsub job.sh`


To check job status:

`qstat`

5. output will be in output.txt
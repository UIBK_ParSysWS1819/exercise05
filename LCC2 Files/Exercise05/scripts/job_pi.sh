#!/bin/bash

#$ -N pi_test

#$ -q std.q

#$ -cwd

#$ -o output_pi.txt

#$ -j yes

#$ -pe openmp 8

./bench_pi.sh

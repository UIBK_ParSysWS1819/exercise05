#!/bin/bash

# monte carlo pi

#module load intel/15.0

printf "|Size|Seq|1T|2T|4T|8T|\n"
printf "|---:|---:|---:|---:|---:|---:|\n"

for size in 10 100 1000 10000 100000 1000000 10000000 100000000 #200000000 500000000
do
    printf "|%s|" $size

    out_seq=`../PI_estimator/exe $size`
    printf "%s ms|" $out_seq

    for threads in 1 2 4 8
    do
        export OMP_NUM_THREADS=$threads
        out_par=`../PI_estimator/exe_omp $size`
        printf "%s ms|" $out_par
    done
    printf "\n"

done

#module unload intel/15.0

#!/bin/bash
# mergesort
printf "|Size|Seq|1T|2T|4T|8T|\n"
printf "|---:|---:|---:|---:|---:|---:|\n"

for size in 100 1000 10000 100000 1000000 10000000 100000000
do
    printf "|%s|" $size

    out_seq=`../Mergesort/mergesort_seq $size`
    printf "%s ms|" $out_seq

    for threads in 1 2 4 8
    do
        export OMP_NUM_THREADS=$threads
        out_par=`../Mergesort/mergesort_omp $size`
        printf "%s ms|" $out_par
    done
    printf "\n"

done

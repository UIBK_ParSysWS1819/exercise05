#!/bin/bash

g++ -std=c++11 -O3 -fopenmp pi_estimator_omp.cpp -o exe_omp
g++ -std=c++11 -O3 pi_estimator_seq.cpp -o exe


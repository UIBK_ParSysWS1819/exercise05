#ifndef EXERCISE04_SQUEENS_H
#define EXERCISE04_SQUEENS_H

#include "Queens.h"
#include <iostream>

class SQueens: public Queens {

public:
    explicit SQueens(unsigned long problem_size)
    : Queens(problem_size) {}

    explicit SQueens() : Queens() {}


    /***
     * Check if queen could be placed safely without intersecting other queens
     * @param x row position of the new possible queen
     * @param y column position of the new possible queen
     * @param solution previously placed queens
     */
    bool is_safe(int x, int y,  const std::vector <int> &solution) const {
        // Check if queen could be placed safely without intersecting other queens
        for( auto row=0; row< x;row++ ){
            if (solution[row] == y ||  // same column
                solution[row] + row == y + x ||  // same diagonal
                solution[row] - row == y - x){   // same diagonal
                return false;
            }
        }
        return true;
    }

    /***
     * Collect all possible solutions for the new chess field extended by one row.
     * The new queen is placed based on the new row for each one of the possible new solutions
     * (if it is not intersecting with the previous solutions).
     */
    std::vector<std::vector <int>> addQueen(int new_row, int columnn, std::vector<std::vector <int>> previous_solutions){
        std::vector<std::vector <int>> new_solutions = std::vector<std::vector <int>>();
        for( auto solution : previous_solutions){
            // Try to place a new queen in each column of the new_row.
            for( int new_column=0; new_column<columnn; new_column++){
//                 std::cout << "test: " << new_column <<" in row "<< new_row << std::endl;
                // Test if there are any conflicts with this possible solution
                if (is_safe(new_row, new_column, solution)){
                    // extend current solution by the new queen
                    solution.push_back(new_column);

                    new_solutions.push_back(solution);

                    // reset the queen from current solution for possible new solutions
                    solution.pop_back();
                }
            }
        }
        return new_solutions;
    }

    /***
     * Adapted implementation of python code from <A HREF="https://de.wikipedia.org/wiki/Damenproblem#Anzahl_der_L%C3%B6sungen_im_klassischen_Damenproblem"> n-queens german wikipedia</a>
     * @param row
     * @param column
     * @return a list of vectors(chessboards) with the queens saved as queens[row] = column of the current queen
     */
    std::vector<std::vector <int>> solve_nqueens(int row, int column){
        if (row <= 0) {
            // base case; if no queen could be set, the empty chess board is the solution
            return std::vector<std::vector <int>>{std::vector <int>{}};
        }
        else
            return addQueen(row - 1, column, solve_nqueens(row - 1, column));
    }

    std::vector<std::vector <int>> solve() override {
        solutions = solve_nqueens(problem_size,problem_size);
        return solutions;
    }

};


#endif //EXERCISE04_SQUEENS_H

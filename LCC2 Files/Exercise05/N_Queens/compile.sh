
rm ./bin/*

echo 'compiling gcc'

module load gcc/8.2.0
g++ -std=c++11 -O3 -fopenmp nqueens_omp.cpp -o ./bin/nqueens_omp
g++ -std=c++11 -O3 nqueens_seq.cpp -o ./bin/nqueens_seq
module unload gcc/8.2.0

echo 'compiling icc'
###
module load intel/15.0
icpc -std=c++11 -O3 -g -openmp nqueens_omp.cpp  -o ./bin/nqueens_omp_icc
icpc -std=c++11 -O3 -g nqueens_seq.cpp -o ./bin/nqueens_seq_icc
module unload intel/15.0

#chmod +x ./bin/*

#!/bin/bash

echo 'compiling gcc'

module load gcc/8.2.0
g++ -std=c++11 -O3 mergesort_seq.cpp -o mergesort_seq
g++ -std=c++11 -O3 -fopenmp mergesort_omp.cpp -o mergesort_omp
module unload gcc/8.2.0

echo 'compiling icc'

module load intel/15.0
icpc -std=c++11 -O3 mergesort_seq.cpp -o mergesort_seq_icc
icpc -std=c++11 -O3 -fopenmp mergesort_omp.cpp -o mergesort_omp_icc
module unload intel/15.0
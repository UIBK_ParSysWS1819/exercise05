#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include "../chrono_timer.h"

#define CUTOFF_MERGESORT 100000


void merge(std::vector<int32_t> const& left_side, std::vector<int32_t> const& right_side, std::vector<int32_t>& values) {
    auto left_begin = left_side.cbegin();
    auto left_end = left_side.cend();

    auto right_begin = right_side.cbegin();
    auto right_end = right_side.cend();

    for(int k = 0; k < values.size(); k++) {
        if(left_begin == left_end) {        // copy rest of right array
            values[k] = *right_begin++;
        }
        else if(right_begin == right_end) { // copy rest of left array
            values[k] = *left_begin++;
        }
        else {
            if(*left_begin <= *right_begin) {
                values[k] = *left_begin++;
            }
            else {
                values[k] = *right_begin++;
            }
        }
    }
}

void mergesort_seq(std::vector<int32_t> &values) {
    int size = values.size();

    if(size <= CUTOFF_MERGESORT) {    // switch sort for small sub-arrays
        std::sort(values.begin(), values.end());
        return;
    }

    auto half = size / 2;
    auto begin = values.begin();
    auto mid = values.begin() + half;
    auto end = values.end();

    std::vector<int32_t> left_side(begin, mid);  // save first half in temporary vector
    std::vector<int32_t> right_side(mid, end);   // save second half in temporary vector

    mergesort_seq(left_side);   // sort left side
    mergesort_seq(right_side);  // sort right side

    merge(left_side, right_side, values);
}

void do_mergesort(std::vector<int32_t> &values) {
    mergesort_seq(values);
}

bool is_sorted(std::vector<int32_t> &values) {
    for(unsigned i = 1; i < values.size(); i++) {
        if(values[i-1] > values[i]) {   // if current value is smaller than previous one, sorting failed
            return false;
        }
    }

    return true;
}

/* Utility functions */

void fill_vector(std::vector<int32_t> &values, int size) {
    // https://en.wikipedia.org/wiki/C%2B%2B11#Extensible_random_number_facility
    // http://itscompiling.eu/2016/04/11/generating-random-numbers-cpp/
    std::random_device random_device;
    std::mt19937 engine(random_device());
    std::uniform_int_distribution<int32_t> uniform_dist(0, 100);

    for(int i = 0; i < size; i++) {
        values.push_back(uniform_dist(engine));
    }
}

void print_values(std::vector<int32_t> &values) {
    for(int value : values) {
        std::cout << value << ", ";
    }
    std::cout << std::endl;
}


long median(std::vector<long> times) {
    int size = times.size();

    if(size % 2 == 0) { // if the size is even, build the average of the middle two.
        long val1 = times[size / 2 - 1];
        long val2 = times[size / 2];

        return (val1 + val2) / 2;
    }
    else {
        return times[size / 2];
    }
}

int main(int argc, char** argv) {
    int size = 100; //default

    if(argc == 2) {
        size = std::stoi(argv[1]);
    }

    //std::cout << "Array size: " << size << std::endl;

    /* We will build the median here of multiple runs and then return this value */
    int runs = 7;
    std::vector<long> times;
    for (int i = 1; i <= runs; i++) {
        //1. fill array with random values
        std::vector<int32_t > values;
        values.reserve(size);

        fill_vector(values, size);

        //std::cout << "Run " << i << ":" << std::endl;
        //print_values(values);

        //2. Do mergesort
        std::string str("Sequential Mergesort run " + std::to_string(i));
        long time = 0l;

        {
            ChronoTimer timer(str);
            do_mergesort(values);
            time = timer.getElapsedTime();
            //std::cout << "Run " << i << ": " << time << " ms" << std::endl;
        }

        times.push_back(time);

        //std::cout << "Timer - " << str << ": " << time << " ms" << std::endl;

        //print_values(values);
        //std::cout << std::endl;

        //3. do check if sorting was successful
        if(!is_sorted(values)) {
            std::cout << "Sorting failed!" << std::endl;

            return EXIT_FAILURE;
        }
    }

    std::sort(times.begin(), times.end());
    //std::cout << "Median is " << median(times) << " ms" << std::endl;
    std::cout << median(times) << std::endl;

    return EXIT_SUCCESS;
}

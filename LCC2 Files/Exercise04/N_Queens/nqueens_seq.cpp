#include <iostream>
#include <chrono>
#include <algorithm>
#include "Queens.h"
#include "SQueens.h"
#include "../chrono_timer.h"

/*
 * Calculating the number of solutions to the N-Queens problem. Calculating the number of solutions to the N-Queens
 * problem. The input parameter for the program is the number N, and its output is the number of possible solutions.
 */

long median(std::vector<long> times) {
    int size = times.size();

    if(size % 2 == 0) { // if the size is even, build the average of the middle two.
        long val1 = times[size / 2 - 1];
        long val2 = times[size / 2];

        return (val1 + val2) / 2;
    }
    else {
        return times[size / 2];
    }
}

int main(int argc, char** argv) {
    int N = 13; //default

    if(argc == 2) {
        N = std::stoi(argv[1]);
    }

//    std::cout << "N: " << N << std::endl;

    SQueens queens = SQueens(N);

    int runs = 7;
    std::vector<long> times;
    for (int i = 1; i <= runs; i++) {
        std::string str("N-Queens Carlo Pi run " + std::to_string(i));
        ChronoTimer timer(str);

        queens.solve();

        long time = timer.getElapsedTime();
        times.push_back(time);

    }

//    std::cout << "Amount solutions: " << queens.get_amount_solutions() << std::endl;

    std::sort(times.begin(), times.end());
    //std::cout << "Median is " << median(times) << " ms" << std::endl;
    std::cout << median(times) << std::endl;

    return EXIT_SUCCESS;
}
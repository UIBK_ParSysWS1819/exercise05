#include <iostream>
#include <math.h>
#include <algorithm>
#include <vector>
#include "pi_estimator.hpp"
#include "../chrono_timer.h"

/*
 * A Monte Carlo estimator for pi. The input should be the number of samples to
 * use in the estimation, the output is the approximation of pi which was computed.
 */

long median(std::vector<long> times) {
    int size = times.size();

    if(size % 2 == 0) { // if the size is even, build the average of the middle two.
        long val1 = times[size / 2 - 1];
        long val2 = times[size / 2];

        return (val1 + val2) / 2;
    }
    else {
        return times[size / 2];
    }
}

int main(int argc, char** argv) {
    int num_samples = 10000000; //default

    if(argc == 2) {
        num_samples = std::stoi(argv[1]);
    }

    double pi = 0.0;

    int runs = 7;
    std::vector<long> times;
    for (int i = 1; i <= runs; i++) {
        ChronoTimer timer("OpenMP Pi Estimator");
        pi = monte_carlo_pi_omp(num_samples);
	    long time = timer.getElapsedTime();
        times.push_back(time);
    }

    std::sort(times.begin(), times.end());
    std::cout << median(times) << std::endl;

    return EXIT_SUCCESS;
}

#ifndef MONTE_CARLO_PI_H
#define MONTE_CARLO_PI_H

#include "stddef.h"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <omp.h>

double getRand(){
    // the rand function isn't very efficient since it blocks on parallel calls,
    // but since we don't care about performance for this exercise, we leave it as is for now
    return (double)rand() / (RAND_MAX);
}

double distance(double x, double y) {
    return x*x + y*y;
}

size_t monte_carlo_hits(size_t num_samples) {
    size_t hits = 0;
    for(size_t i = 0; i < num_samples; i++) {
        double x = getRand();
        double y = getRand();
        double dist = distance(x, y);
        if(dist <= 1) {
            hits++;
        }
    }
    return hits;
}

double calculate_pi(size_t samples,size_t hits) {
    return ((double)hits * 4.0) / (double)samples;
}

double monte_carlo_pi_seq(size_t num_samples){
    srand(time(NULL));
    size_t hits = monte_carlo_hits(num_samples);
    double pi = calculate_pi(num_samples, hits);
    return pi;
}

double monte_carlo_pi_omp(size_t num_samples){
    srand(time(NULL));
    size_t hits = 0;
    #pragma omp parallel reduction(+:hits)
    {
        hits = monte_carlo_hits(num_samples / omp_get_num_threads());
    }
    double pi = calculate_pi(num_samples, hits);
    return pi;
}


#endif
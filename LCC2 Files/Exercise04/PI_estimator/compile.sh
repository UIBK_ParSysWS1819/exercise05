#!/bin/bash

g++ -std=c++11 -O3 -fopenmp pi_estimator_omp.cpp -o exe
g++ -std=c++11 -O3 -fopenmp pi_estimator_seq.cpp -o exe_omp


#!/bin/bash

module load intel/15.0

# N-Queens problem
printf "|Size|Seq|1T|2T|4T|8T|\n"
printf "|---:|---:|---:|---:|---:|---:|\n"

for size in 8 10 12
do
    printf "|%s|" $size

    out_seq=`../N_Queens/bin/nqueens_seq_icc $size`
    printf "%s ms|" $out_seq

    for threads in 1 2 4 8
    do
        export OMP_NUM_THREADS=$threads
        out_par=`../N_Queens/bin/nqueens_omp_icc $size`
        printf "%s ms|" $out_par
    done
    printf "\n"

done

module unload intel/15.0

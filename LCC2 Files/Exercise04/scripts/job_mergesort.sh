#!/bin/bash

#$ -N mergesort_test

#$ -q std.q

#$ -cwd

#$ -o output_mergesort.txt

#$ -j yes

#$ -pe openmp 8

./bench_mergesort.sh
./bench_mergesort_icc.sh

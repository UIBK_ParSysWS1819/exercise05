#!/bin/bash

#$ -N queens_test

#$ -q std.q

#$ -cwd

#$ -o output_queens.txt

#$ -j yes

#$ -pe openmp 8

./bench_queens.sh
./bench_queens_icc.sh

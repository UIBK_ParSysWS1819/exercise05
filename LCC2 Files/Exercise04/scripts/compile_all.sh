#!/bin/bash

chmod +x ../N_Queens/compile.sh
chmod +x ../PI_estimator/compile.sh
chmod +x ../Mergesort/compile.sh

cd ../N_Queens/
sh compile.sh
cd ../PI_estimator/
sh compile.sh
cd ../Mergesort/
sh compile.sh

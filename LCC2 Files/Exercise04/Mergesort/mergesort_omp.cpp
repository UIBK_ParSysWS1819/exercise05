#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include "../chrono_timer.h"
#include "Mergesort.h"


// ~ 2h

/*
 * Merge sort. The input for your program should be the size of the array to sort. The generated array
 * should be filled randomly. The output is the result of a check that the array is actually sorted.
 */

long median(std::vector<long> times) {
    int size = times.size();

    if(size % 2 == 0) { // if the size is even, build the average of the middle two.
        long val1 = times[size / 2 - 1];
        long val2 = times[size / 2];

        return (val1 + val2) / 2;
    }
    else {
        return times[size / 2];
    }
}

int main(int argc, char** argv) {
    int size = 100; //default

    if(argc == 2) {
        size = std::stoi(argv[1]);
    }

    //std::cout << "Array size: " << size << std::endl;

    /* We will build the median here of multiple runs and then return this value */
    int runs = 7;
    std::vector<long> times;
    for (int i = 1; i <= runs; i++) {
        //1. fill array with random values
        Mergesort mergesort(size);
        mergesort.fill_vector();
        //mergesort.print_values();

        //2. Do mergesort_seq
        std::string str("OpenMP Mergesort run " + std::to_string(i));
        long time = 0l;

        #pragma omp parallel
        #pragma omp single    // start with one thread of the pool
        {
            ChronoTimer timer("OpenMP Mergesort");
            mergesort.do_mergesort();
            time = timer.getElapsedTime();
            //std::cout << "Run " << i << ": " << time << " ms" << std::endl;
        }

        times.push_back(time);

        //3. do check if sorting was successful
        if(!mergesort.is_sorted()) {
            std::cout << "Sorting failed!" << std::endl;

            return EXIT_FAILURE;
        }
    }

    std::sort(times.begin(), times.end());
    //std::cout << "Median is " << median(times) << " ms" << std::endl;
    std::cout << median(times) << std::endl;

    return EXIT_SUCCESS;
}
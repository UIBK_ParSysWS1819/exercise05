#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>
#include <omp.h>
#include <random>
#include "../chrono_timer.h"

/*
 * A Monte Carlo estimator for pi. The input should be the number of samples to
 * use in the estimation, the output is the approximation of pi which was computed.
 */

size_t monte_carlo_hits(size_t num_samples) {
    std::random_device random_device;
    std::ranlux48_base engine(random_device());
    std::uniform_real_distribution<double> uniform_dist(0, 1);
    size_t hits = 0;
    for(size_t i = 0; i < num_samples; i++) {
        double x = uniform_dist(engine);
        double y = uniform_dist(engine);
        double dist = x*x + y*y;
        if(dist <= 1) {
            hits++;
        }
    }
    return hits;
}

double monte_carlo_pi_omp(size_t num_samples){
    size_t hits = 0;
    #pragma omp parallel reduction(+:hits)
    {
        hits = monte_carlo_hits(num_samples / omp_get_num_threads());
    }
    double pi = ((double)hits * 4.0) / (double)num_samples;
    return pi;
}

long median(std::vector<long> times) {
    int size = times.size();

    if(size % 2 == 0) { // if the size is even, build the average of the middle two.
        long val1 = times[size / 2 - 1];
        long val2 = times[size / 2];

        return (val1 + val2) / 2;
    }
    else {
        return times[size / 2];
    }
}

int main(int argc, char** argv) {
    int num_samples = 100000000; //default

    if(argc == 2) {
        num_samples = std::stoi(argv[1]);
    }

    std::cout.precision(10);

    //std::cout << "Number of samples: " << num_samples << std::endl;
    //std::cout << "Actual Pi: " << M_PI << std::endl;

    int runs = 7;
    std::vector<long> times;
    for (int i = 1; i <= runs; i++) {
        std::string str("Parallel Monte Carlo Pi run " + std::to_string(i));
        ChronoTimer timer(str);

        double pi = monte_carlo_pi_omp(num_samples);

        long time = timer.getElapsedTime();
        times.push_back(time);

        //std::cout << "Timer - " << str << ": " << time << " ms" << std::endl;
        //std::cout << "Calculated Pi: " << pi << std::endl;
    }

    std::sort(times.begin(), times.end());
    //std::cout << "Median is " << median(times) << " ms" << std::endl;
    std::cout << median(times) << std::endl;

    return EXIT_SUCCESS;
}
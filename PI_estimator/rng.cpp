#include <iostream>
#include <random>
#include <omp.h>

#include "chrono_timer.h"

int rdrand64_step (uint64_t *rand) {
    unsigned char ok;
    asm volatile ("rdrand %0; setc %1"
    : "=r" (*rand), "=qm" (ok));
    return (int) ok;
}

int main(int argc, char** argv) {

    double sum = 0;

    int times = 100000000;

    for(int k = 0 ; k < 4; k++) {
        {
            ChronoTimer timer("default");
            #pragma omp parallel
            {
                std::default_random_engine engine(omp_get_thread_num());
                std::uniform_real_distribution<double> uniform_dist(0, 1);
                for (int i = 0; i < times; i++) {
                    double r = uniform_dist(engine);
                    sum += r;
                }
            }
        }
        {
            ChronoTimer timer("mt19937");
            #pragma omp parallel
            {
                std::random_device random_device;
                std::mt19937 engine(random_device());
                std::uniform_real_distribution<double> uniform_dist(0, 1);
                for (int i = 0; i < times; i++) {
                    double r = uniform_dist(engine);
                    sum += r;
                }
            }
        }
        {
            ChronoTimer timer("mt19937_64");
            #pragma omp parallel
            {
                std::random_device random_device;
                std::mt19937_64 engine(random_device());
                std::uniform_real_distribution<double> uniform_dist(0, 1);
                for (int i = 0; i < times; i++) {
                    double r = uniform_dist(engine);
                    sum += r;
                }
            }
        }
        {
            ChronoTimer timer("minstd_rand");
            #pragma omp parallel
            {
                std::random_device random_device;
                std::minstd_rand engine(random_device());
                std::uniform_real_distribution<double> uniform_dist(0, 1);
                for (int i = 0; i < times; i++) {
                    double r = uniform_dist(engine);
                    sum += r;
                }
            }
        }
        {
            ChronoTimer timer("minstd_rand0");
            #pragma omp parallel
            {
                std::random_device random_device;
                std::minstd_rand0 engine(random_device());
                std::uniform_real_distribution<double> uniform_dist(0, 1);
                for (int i = 0; i < times; i++) {
                    double r = uniform_dist(engine);
                    sum += r;
                }
            }
        }
        {
            ChronoTimer timer("ranlux48");
            #pragma omp parallel
            {
                std::random_device random_device;
                std::ranlux48 engine(random_device());
                std::uniform_real_distribution<double> uniform_dist(0, 1);
                for (int i = 0; i < times; i++) {
                    double r = uniform_dist(engine);
                    sum += r;
                }
            }
        }
        {
            ChronoTimer timer("ranlux48_base");
            #pragma omp parallel
            {
                std::random_device random_device;
                std::ranlux48_base engine(random_device());
                std::uniform_real_distribution<double> uniform_dist(0, 1);
                for (int i = 0; i < times; i++) {
                    double r = uniform_dist(engine);
                    sum += r;
                }
            }
        }
        {
            ChronoTimer timer("ranlux24");
            #pragma omp parallel
            {
                std::random_device random_device;
                std::ranlux24 engine(random_device());
                std::uniform_real_distribution<double> uniform_dist(0, 1);
                for (int i = 0; i < times; i++) {
                    double r = uniform_dist(engine);
                    sum += r;
                }
            }
        }
        {
            ChronoTimer timer("ranlux24_base");
            #pragma omp parallel
            {
                std::random_device random_device;
                std::ranlux24_base engine(random_device());
                std::uniform_real_distribution<double> uniform_dist(0, 1);
                for (int i = 0; i < times; i++) {
                    double r = uniform_dist(engine);
                    sum += r;
                }
            }
        }
	// from https://software.intel.com/en-us/articles/intel-digital-random-number-generator-drng-software-implementation-guide
	{
            ChronoTimer timer("asm intel rand");
            #pragma omp parallel
            {
                for (int i = 0; i < times/10; i++) {
                    uint64_t a;
                    rdrand64_step(&a);
                    sum += (double) a / UINT64_MAX;
                }
            }
        }
        std::cout << std::endl;
    }

    std::cout << sum << std::endl;

    return EXIT_SUCCESS;
}

(Also tested but not included since they were really outperformed: ranlux48, ranlux24)

Sequential: 

Timer - rand: 1317 ms 
Timer - rand_s: 1085 ms 
Timer - default: 2089 ms 
Timer - mt19937: 2128 ms 
Timer - mt19937_64: 1636 ms 
Timer - minstd_rand: 2089 ms 
Timer - minstd_rand0: 2089 ms 
Timer - ranlux48_base: 1997 ms 
Timer - ranlux24_base: 1729 ms 

Timer - rand: 1317 ms 
Timer - rand_s: 1084 ms 
Timer - default: 2089 ms 
Timer - mt19937: 2127 ms 
Timer - mt19937_64: 1636 ms 
Timer - minstd_rand: 2089 ms 
Timer - minstd_rand0: 2089 ms 
Timer - ranlux48_base: 1997 ms 
Timer - ranlux24_base: 1729 ms 

Timer - rand: 1317 ms 
Timer - rand_s: 1085 ms 
Timer - default: 2089 ms 
Timer - mt19937: 2127 ms 
Timer - mt19937_64: 1636 ms 
Timer - minstd_rand: 2089 ms 
Timer - minstd_rand0: 2089 ms 
Timer - ranlux48_base: 1997 ms 
Timer - ranlux24_base: 1729 ms 

Timer - rand: 1317 ms 
Timer - rand_s: 1084 ms 
Timer - default: 2089 ms 
Timer - mt19937: 2127 ms 
Timer - mt19937_64: 1636 ms 
Timer - minstd_rand: 2089 ms 
Timer - minstd_rand0: 2089 ms 
Timer - ranlux48_base: 1997 ms 
Timer - ranlux24_base: 1729 ms 


------------------------


Parallel:

Timer - default : 2077 ms
Timer - mt19937 : 55056 ms
Timer - mt19937_64 : 46831 ms
Timer - minstd_rand : 2082 ms
Timer - minstd_rand0 : 2072 ms
Timer - knuth_b : 4270 ms
Timer - ranlux48: 38943 ms
Timer - ranlux48_base: 1186 ms
Timer - ranlux24: 19222 ms
Timer - ranlux24_base: 1787 ms


Timer - default : 2074 ms
Timer - mt19937 : 55472 ms
Timer - mt19937_64 : 46810 ms
Timer - minstd_rand : 2088 ms
Timer - minstd_rand0 : 2464 ms
Timer - knuth_b : 4341 ms
Timer - ranlux48: 40843 ms
Timer - ranlux48_base: 1186 ms
Timer - ranlux24: 18118 ms
Timer - ranlux24_base: 1742 ms


Timer - default : 2140 ms
Timer - mt19937 : 55612 ms
Timer - mt19937_64 : 46951 ms
Timer - minstd_rand : 2082 ms
Timer - minstd_rand0 : 2070 ms
Timer - knuth_b : 4271 ms
Timer - ranlux48: 38984 ms
Timer - ranlux48_base: 1180 ms
Timer - ranlux24: 18114 ms
Timer - ranlux24_base: 1742 ms


Timer - default : 2074 ms
Timer - mt19937 : 55751 ms
Timer - mt19937_64 : 46226 ms
Timer - minstd_rand : 2098 ms
Timer - minstd_rand0 : 2073 ms
Timer - knuth_b : 4305 ms
Timer - ranlux48: 38546 ms
Timer - ranlux48_base: 1180 ms
Timer - ranlux24: 18118 ms
Timer - ranlux24_base: 1742 ms 

